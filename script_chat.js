
/* VARIÁVEIS DO CHAT */
let input = document.querySelector("textarea")
let visor = document.querySelector(".visor_fundo")


/* EVENTO DE ENVIO DA MENSAGEM */
let btn_enviar = document.querySelector(".btn_enviar")
btn_enviar.addEventListener("click",enviar)

/* DUNÇÃO DO ENVIO DA MENSAGEM */
function enviar () {
    let caixa = document.createElement("div");
    let texto = document.createElement("p");
    texto.classList.add("visor_topo")
    texto.innerText = input.value;
    let btn1 = document.createElement("button");
    btn1.innerText = "Excluir";
    btn1.classList.add("btn_excluir");
    btn1.setAttribute("onclick","excluir(this)");
    let btn2 = document.createElement("button");
    btn2.innerText = "Editar";
    btn2.classList.add("btn_editar");
    btn2.setAttribute("onclick", "editar(this)");
    caixa.append(texto);
    caixa.append(btn1);
    caixa.append(btn2);
    visor.append(caixa);
    document.getElementById('box_texto').value = "";
}

/* FUNÇÃO DA EXCLUSÃO DA MENSAGEM */
function excluir(mensagem) {
    mensagem.parentNode.remove();    
}

/* FUNÇÃO DA EDIÇÃO DA MENSGAEM */
function editar(mensagem) {
    let div = document.createElement("div")
    let caixa = document.createElement("input");
    caixa.classList.add("texto_edit");
    let botao = document.createElement("button");
    botao.classList.add("enviar");
    botao.innerText = "Enviar";
    div.append(caixa);
    div.append(botao)
    mensagem.parentNode.append(div);
    botao.addEventListener("click", enviar_edit)
}

/* FUNÇÃO DO ENVIO DA EDIÇÃO */
function enviar_edit () {
    let input = document.querySelector(".texto_edit");
    input.parentNode.parentNode.innerHTML = "<div><p class='visor_topo'>" + input.value + "</p></div><button class='btn_excluir' onclick='excluir(this)'>Excluir</button><button class='btn_editar' onclick='editar(this)'>Editar</button>"
}
